# Tip Calculator

Dead simple tip calculator that computes tip based on the subtotal, pre-tax. It's also my first app, so some things are missing that I don't know how to implement just yet.

## License

AGPLv3+, see LICENSE.md.
