package quest.faraday.tip_calculator

import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.databinding.DataBindingUtil
import quest.faraday.tip_calculator.databinding.ActivityMainBinding
import java.math.BigDecimal
import java.math.RoundingMode

class MainActivity : AppCompatActivity() {
	private lateinit var binding: ActivityMainBinding

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
//		setContentView(R.layout.activity_main)
//		binding = ActivityMainBinding.bind(findViewById(R.id.root))
		binding.calculateButton.setOnClickListener { calculate() }
		binding.gratuityEdit.onDone { calculate() }
	}

	private fun calculate() {
		try {
			val subtotal = BigDecimal(binding.subtotalEdit.text.toString())
			val tax = BigDecimal(binding.taxEdit.text.toString())
			val gratuityCoeff = BigDecimal(binding.gratuityEdit.text.toString())
			val tip = gratuityCoeff * subtotal
			val saved = (gratuityCoeff * (subtotal + tax) - tip).setScale(2, RoundingMode.HALF_UP)
			val total = (subtotal+tax+tip).setScale(2, RoundingMode.HALF_UP)
			displayMessage("Tip: $${tip.setScale(2, RoundingMode.HALF_UP)}\nTotal: $${total}\nYou saved: $${saved}")
		} catch (e: NumberFormatException) {
			displayMessage("Invalid number format")
		}
	}

	private fun displayMessage(message: String) {
		hideKeyboard()
		binding.resultsText.text = message
		binding.resultsText.visibility = View.VISIBLE
	}

	private fun hideKeyboard() {
		val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
		imm.hideSoftInputFromWindow(binding.root.windowToken, 0)
	}
}

fun EditText.onDone(callback: () -> Unit) {
	setOnEditorActionListener { _, actionId, _ ->
		if (actionId == EditorInfo.IME_ACTION_DONE) {
			callback.invoke()
		}
		false
	}
}